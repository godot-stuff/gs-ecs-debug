extends Component
class_name Debug

"""
	A Generic Node for Debugging.

	It contains a reference to the Entity
	it is debugging.
"""

var entity
var nodes: Dictionary = {}
