extends System
class_name DebugSystem 

@export var DEBUG_NODE_SCENE : PackedScene

var scene

func on_process(entities, delta):

	for entity in entities:

		var _debug = entity.get_component("debug") as Debug

		# we have a debug node, mark it for updating

		var node = get_debug_node(_debug)

		if node:
			node.queue_redraw()
			continue

		# create debug node

		var _node = scene.instantiate()
		_node.name = "!Debug[%s][%s]" % [self.name, entity.name]
		add_child(_node, true)
		_debug.entity = entity
		_debug.nodes[get_instance_id()] = _node
		_node.debug = _debug
		_node.entity = entity


func get_debug_node(debug: Debug):
	if debug.nodes.has(get_instance_id()):
		return debug.nodes[get_instance_id()]


func on_ready():

	if !DEBUG_NODE_SCENE:
		Logger.error("- missing debug scene")
		get_tree().quit()

	scene = DEBUG_NODE_SCENE
