extends Node2D
class_name DebugNode

"""
	Class: DebugNode

	A Base Debugging Node that can be attached to a DebugSystem

	Remarks:

		This Class contains a number of Helper methods for
		displaying information for an Entity
"""

@onready var debug_font_small: Font = preload("res://addons/gs_ecs_debug/font/debug_8.tres")
@onready var debug_font_normal: Font = preload("res://addons/gs_ecs_debug/font/debug_16.tres")

var debug: Debug
var entity: Entity : set = _set_entity, get = _get_entity
var text: Array

var DEBUG_COLOR_TEXT_1 = Color("white")
var DEBUG_COLOR_TEXT_2 = Color("red")
var DEBUG_COLOR_TEXT_3 = Color("green")
var DEBUG_COLOR_TEXT_4 = Color("blue")
var DEBUG_COLOR_TEXT_5 = Color("yellow")
var DEBUG_COLOR_TEXT_6 = Color("purple")

var DEBUG_COLOR_LINE_1 = Color("white")
var DEBUG_COLOR_LINE_2 = Color("red")
var DEBUG_COLOR_LINE_3 = Color("green")
var DEBUG_COLOR_LINE_4 = Color("blue")
var DEBUG_COLOR_LINE_5 = Color("yellow")
var DEBUG_COLOR_LINE_6 = Color("purple")


func draw_circle_arc(center, radius, angle_from, angle_to, color = DEBUG_COLOR_LINE_1):
	var nb_points = 32
	var points_arc = PackedVector2Array()

	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	for index_point in range(nb_points):
		draw_line(points_arc[index_point], points_arc[index_point + 1], color)


func draw_circle_arc_poly(center, radius, angle_from, angle_to, color = DEBUG_COLOR_LINE_1):
	var nb_points = 32
	var points_arc = PackedVector2Array()
	points_arc.push_back(center)
	var colors = PackedColorArray([color])

	for i in range(nb_points + 1):
		var angle_point = deg_to_rad(angle_from + i * (angle_to - angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)
	draw_polygon(points_arc, colors)


func draw_text(pos, text, color = DEBUG_COLOR_TEXT_1, font = debug_font_normal):

	var height = 12
	var y = 0
	for i in range(text.size()):
		draw_string(font, pos + Vector2(15,y), text[i],HORIZONTAL_ALIGNMENT_LEFT, -1, height, color)
		y += height + 5


func on_draw() -> void:
	draw_text(entity.position, [entity.name, str(entity.get_instance_id())])


func _get_entity():
	return entity
	
	
func _set_entity(entity_: Entity):
	entity = entity_
	entity.connect("tree_exited", _on_exit_tree)
	

func _on_exit_tree():
	queue_free()	
	
	
func _draw() -> void:
	on_draw()


