extends Component
class_name Movable

@export var SPEED : int = 5
@export var SPEED_FACTOR : int = 10
@export var DISTANCE : int = 100


var origin = Vector2()
var distance = 100
var speed = 0
var speed_factor = 10
var moveto_pos = Vector2()
var direction = 1


func _ready():

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR
	if DISTANCE:		distance = DISTANCE
